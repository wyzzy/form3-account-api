module form3-account-api

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/jarcoal/httpmock v1.0.8
	github.com/stretchr/testify v1.7.0
)
