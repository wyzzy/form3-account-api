package form3

import (
	"encoding/json"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

type mockClientReturningError struct{}
type mockError struct{}
func (e mockError) Error() string{
	return "error!"
}
func (c *mockClientReturningError) Post(string, interface{}) (*http.Response, error){
	return nil, mockError{}
}
func (c *mockClientReturningError) Delete(string) (*http.Response, error){
	return nil, mockError{}
}
func (c *mockClientReturningError) Get(string) (*http.Response, error){
	return nil, mockError{}
}

// ---- begin CREATE

func TestCreateAccountWith4xxStatusCodeButInvalidJson(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "11
 }
`
	httpmock.RegisterResponder("POST", "http://localhost:8080/v1/organisation/accounts",
		httpmock.NewStringResponder(400, mockResponse))

	client := ConcreteClient()
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "invalid json", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestCreateAccountWith4xxStatusCodeButWrongBody(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "data": 123
 }
`
	httpmock.RegisterResponder("POST", "http://localhost:8080/v1/organisation/accounts",
		httpmock.NewStringResponder(400, mockResponse))

	client := ConcreteClient()
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "", err.ErrorMessage)
	assert.EqualValues(t, 400, err.StatusCode)
}

func TestCreateAccountWith4xxStatusCode(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "error_message": "this is an error"
 }
`
	httpmock.RegisterResponder("POST", "http://localhost:8080/v1/organisation/accounts",
		httpmock.NewStringResponder(400, mockResponse))

	client := ConcreteClient()
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "this is an error", err.ErrorMessage)
	assert.EqualValues(t, 400, err.StatusCode)
}

func TestCreateAccountSuccessWithInvalidResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "data": 123
 }
`
	httpmock.RegisterResponder("POST", "http://localhost:8080/v1/organisation/accounts",
		httpmock.NewStringResponder(200, mockResponse))

	client := ConcreteClient()
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "invalid json structure", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestCreateAccountWithErrorFromClient(t *testing.T) {
	client := mockClientReturningError{}
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "error!", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestCreateAccountSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	expectedResponse := AccountResponse{
		Data: AccountResponseData{
			Type: "accounts",
			Id: "ad27e265-9605-4b4b-a0e5-3003ea9cc4dc",
			OrganisationId: "eb0bd6f5-c3f5-44b2-b677-acd23cdde73c",
			Attributes: AccountResponseAttributes{
				Country: "GB",
				BaseCurrency: "GBP",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}
	bytes, _ := json.Marshal(expectedResponse)
	httpmock.RegisterResponder("POST", "http://localhost:8080/v1/organisation/accounts",
		httpmock.NewStringResponder(200, string(bytes)))

	client := ConcreteClient()
	response, err := CreateAccount(&client, &CreateAccountRequest{})

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, &expectedResponse, response)
}

// ----end CREATE

// ----begin DELETE

func TestDeleteAccountSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("DELETE", "http://localhost:8080/v1/organisation/accounts/111?version=0",
		httpmock.NewBytesResponder(204, nil))

	client := ConcreteClient()
	response, err := DeleteAccount(&client, "111", "0")

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, "success", response.Message)
}

func TestDeleteAccountNotSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	mockResponse := `
{
   "error_message": "this is an error"
 }
`
	httpmock.RegisterResponder("DELETE", "http://localhost:8080/v1/organisation/accounts/111?version=0",
		httpmock.NewStringResponder(400, mockResponse))

	client := ConcreteClient()
	response, err := DeleteAccount(&client, "111", "0")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "this is an error", err.ErrorMessage)
	assert.EqualValues(t, 400, err.StatusCode)
}

func TestDeleteAccountNotSuccessulWithMalformedJson(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	mockResponse := `
{
   "error_mes
 }
`
	httpmock.RegisterResponder("DELETE", "http://localhost:8080/v1/organisation/accounts/111?version=0",
		httpmock.NewStringResponder(400, mockResponse))

	client := ConcreteClient()
	response, err := DeleteAccount(&client, "111", "0")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "invalid json", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestDeleteAccountWithErrorFromClient(t *testing.T) {
	client := mockClientReturningError{}
	response, err := DeleteAccount(&client, "111", "0")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "error!", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

// ----end DELETE

// ----begin FETCH

func TestFetchAccountSuccess(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	expectedResponse := AccountResponse{
		Data: AccountResponseData{
			Type: "accounts",
			Id: "ad27e265-9605-4b4b-a0e5-3003ea9cc4dc",
			OrganisationId: "eb0bd6f5-c3f5-44b2-b677-acd23cdde73c",
			Attributes: AccountResponseAttributes{
				Country: "GB",
				BaseCurrency: "GBP",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}
	bytes, _ := json.Marshal(expectedResponse)
	httpmock.RegisterResponder("GET", "http://localhost:8080/v1/organisation/accounts/1",
		httpmock.NewStringResponder(200, string(bytes)))

	client := ConcreteClient()
	response, err := FetchAccount(&client, "1")

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, &expectedResponse, response)
}

func TestFetchAccountReturnsErrorMessage(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "error_message": "record ad27e265-9605-4b4b-a0e5-3003ea9cc4d1 does not exist"
 }
`
	httpmock.RegisterResponder("GET", "http://localhost:8080/v1/organisation/accounts/1",
		httpmock.NewStringResponder(404, mockResponse))

	client := ConcreteClient()
	response, err := FetchAccount(&client, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "record ad27e265-9605-4b4b-a0e5-3003ea9cc4d1 does not exist", err.ErrorMessage)
	assert.EqualValues(t, 404, err.StatusCode)
}

func TestFetchAccountReturnsMalformedJson(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "error_messag
 }
`
	httpmock.RegisterResponder("GET", "http://localhost:8080/v1/organisation/accounts/1",
		httpmock.NewStringResponder(404, mockResponse))

	client := ConcreteClient()
	response, err := FetchAccount(&client, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "invalid json", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestFetchAccountSuccessWithInvalidResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockResponse := `
{
   "data": 123
 }
`
	httpmock.RegisterResponder("GET", "http://localhost:8080/v1/organisation/accounts/1",
		httpmock.NewStringResponder(200, mockResponse))

	client := ConcreteClient()
	response, err := FetchAccount(&client, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "invalid json structure", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}

func TestFetchAccountWithErrorFromClient(t *testing.T) {
	client := mockClientReturningError{}
	response, err := FetchAccount(&client, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "error!", err.ErrorMessage)
	assert.EqualValues(t, 500, err.StatusCode)
}