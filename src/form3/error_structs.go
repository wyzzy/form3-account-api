package form3

type ApiErrorResponse struct {
	ErrorMessage string `json:"error_message"`
	StatusCode int `json:"status_code"`
}
