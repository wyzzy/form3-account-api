package form3

type CreateAccountRequest struct {
	Data AccountRequestData `json:"data"`
}

type AccountRequestData struct {
	Type           string                   `json:"type"`
	Id             string                   `json:"id"`
	OrganisationId string                   `json:"organisation_id"`
	Attributes     AccountRequestAttributes `json:"attributes"`
}

type AccountRequestAttributes struct {
	Country       string `json:"country"`
	BaseCurrency  string `json:"base_currency"`
	BankId        string `json:"bank_id"`
	BankIdCode    string `json:"bank_id_code"`
	Bic           string `json:"bic"`
	AccountNumber string `json:"account_number"`
	IBAN          string `json:"iban"`
}

type AccountResponse struct {
	Data AccountResponseData `json:"data"`
}

type AccountResponseData struct {
	Type           string                    `json:"type"`
	Id             string                    `json:"id"`
	OrganisationId string                    `json:"organisation_id"`
	Version        int                       `json:"version"`
	Attributes     AccountResponseAttributes `json:"attributes"`
}

type AccountResponseAttributes struct {
	Country       string `json:"country"`
	BaseCurrency  string `json:"base_currency"`
	BankId        string `json:"bank_id"`
	BankIdCode    string `json:"bank_id_code"`
	Bic           string `json:"bic"`
	AccountNumber string `json:"account_number"`
	IBAN          string `json:"iban"`
	Status        string `json:"status"`
}

type DeleteAccountResponse struct{
	Message string
}
