package form3

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

type RestClient interface{
	Post(string, interface{}) (*http.Response, error)
	Delete(string)(*http.Response, error)
	Get(string)(*http.Response, error)
}

type Client struct{
	HOST string
}

func ConcreteClient() Client{
	envHost := os.Getenv("FORM3_API_HOST")
	if envHost != "" {
		return Client{HOST: envHost}
	}

	return Client{HOST: "localhost"}
}

func (c *Client) Post(url string, body interface{}) (*http.Response, error){
	jsonBytes, err := json.Marshal(body)

	if err != nil {
		return nil, err
	}

	return c.performRequest(url, http.MethodPost, jsonBytes)
}

func (c *Client) Delete(url string) (*http.Response, error){
	return c.performRequest(url, http.MethodDelete, []byte{})
}

func (c *Client) Get(url string) (*http.Response, error){
	return c.performRequest(url, http.MethodGet, []byte{})
}

func (c *Client) performRequest(url string, method string, bodyBytes []byte) (*http.Response, error){
	body := bytes.NewReader(bodyBytes)
	fullURL := fmt.Sprintf("http://%s:8080/%s", c.HOST, url)
	req, _ := http.NewRequest(method, fullURL, body)
	client := http.Client{}
	resp, err := client.Do(req)
	return resp, err
}