package form3

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	BaseURL = "v1/organisation/accounts"
)

func CreateAccount(c RestClient, request *CreateAccountRequest) (*AccountResponse, *ApiErrorResponse) {
	response, err := c.Post(BaseURL, request)

	if err != nil {
		errResponse := ApiErrorResponse{
			ErrorMessage: err.Error(),
			StatusCode:   http.StatusInternalServerError,
		}
		return nil, &errResponse
	}

	//don't close body until function returns
	defer response.Body.Close()

	return decodeAccountResponse(response)
}

func DeleteAccount(c RestClient, accountId string, version string) (*DeleteAccountResponse, *ApiErrorResponse) {
	url := fmt.Sprintf(BaseURL+"/%s?version=%s", accountId, version)
	response, err := c.Delete(url)

	if err != nil {
		errResponse := ApiErrorResponse{
			ErrorMessage: err.Error(),
			StatusCode:   http.StatusInternalServerError,
		}
		return nil, &errResponse
	}

	defer response.Body.Close()

	if response.StatusCode != 204 {
		var errResponse ApiErrorResponse
		if err = json.NewDecoder(response.Body).Decode(&errResponse); err != nil {
			return nil, &ApiErrorResponse{
				ErrorMessage: "invalid json",
				StatusCode:   http.StatusInternalServerError,
			}
		}
		errResponse.StatusCode = response.StatusCode
		return nil, &errResponse
	}

	result := DeleteAccountResponse{Message: "success"}
	return &result, nil
}

func FetchAccount(c RestClient, accountId string) (*AccountResponse, *ApiErrorResponse) {
	url := fmt.Sprintf(BaseURL+"/%s?version", accountId)
	response, err := c.Get(url)

	if err != nil {
		errResponse := ApiErrorResponse{
			ErrorMessage: err.Error(),
			StatusCode:   http.StatusInternalServerError,
		}
		return nil, &errResponse
	}

	defer response.Body.Close()

	return decodeAccountResponse(response)
}

func decodeAccountResponse(response *http.Response) (*AccountResponse, *ApiErrorResponse){
	if response.StatusCode > 299 {
		var errResponse ApiErrorResponse
		if err := json.NewDecoder(response.Body).Decode(&errResponse); err != nil {
			return nil, &ApiErrorResponse{
				ErrorMessage: "invalid json",
				StatusCode:   http.StatusInternalServerError,
			}
		}
		errResponse.StatusCode = response.StatusCode
		return nil, &errResponse
	}

	var result AccountResponse
	if err := json.NewDecoder(response.Body).Decode(&result); err != nil {
		fmt.Println(result)
		return nil, &ApiErrorResponse{
			ErrorMessage: "invalid json structure",
			StatusCode:   http.StatusInternalServerError,
		}
	}
	return &result, nil
}
