package form3

import (
	"github.com/stretchr/testify/assert"
	_ "github.com/stretchr/testify/assert"
	"testing"
)

func TestPostWithFailedMarshall(t *testing.T){
	body := map[string]interface{}{
		"foo": make(chan int),
	}
	c := Client{}
	response, err := c.Post("url", body)

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, "json: unsupported type: chan int", err.Error())
}
