package integration

import (
	"fmt"
	"form3-account-api/src/form3"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
)

func TestCreateAccountError(t *testing.T){
	client := form3.ConcreteClient()
	response, err := form3.CreateAccount(&client, &form3.CreateAccountRequest{Data: form3.AccountRequestData{}})

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 400, err.StatusCode)
	assert.Regexp(t, regexp.MustCompile("validation failure list"), err.ErrorMessage)
}

func TestCreateAccountSuccessGB(t *testing.T){
	id := uuid.New().String()
	organisationId := uuid.New().String()
	accRequest := form3.CreateAccountRequest{
		Data: form3.AccountRequestData{
			Id: id,
			OrganisationId: organisationId,
			Type: "accounts",
			Attributes: form3.AccountRequestAttributes{
				Country: "GB",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}

	client := form3.ConcreteClient()
	response, err := form3.CreateAccount(&client, &accRequest)

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, id, response.Data.Id)
	assert.EqualValues(t, organisationId, response.Data.OrganisationId)
	assert.EqualValues(t, 0, response.Data.Version)
	assert.EqualValues(t, "NWBKGB22", response.Data.Attributes.Bic)
	assert.EqualValues(t, "GB", response.Data.Attributes.Country)
	//...
}

func TestDeleteAccountError(t *testing.T){
	client := form3.ConcreteClient()
	response, err := form3.DeleteAccount(&client, "111", "0")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 400, err.StatusCode)
	assert.EqualValues(t, "id is not a valid uuid", err.ErrorMessage)
}

func TestDeleteAccountInvalidID(t *testing.T){
	client := form3.ConcreteClient()
	response, err := form3.DeleteAccount(&client, "111", "0")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 400, err.StatusCode)
	assert.EqualValues(t, "id is not a valid uuid", err.ErrorMessage)
}

func TestDeleteAccountWrongVersion(t *testing.T){
	id := uuid.New().String()
	organisationId := uuid.New().String()
	accRequest := form3.CreateAccountRequest{
		Data: form3.AccountRequestData{
			Id: id,
			OrganisationId: organisationId,
			Type: "accounts",
			Attributes: form3.AccountRequestAttributes{
				Country: "GB",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}

	client := form3.ConcreteClient()
	form3.CreateAccount(&client, &accRequest)
	response, err := form3.DeleteAccount(&client, id, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 404, err.StatusCode)
	assert.EqualValues(t, "invalid version", err.ErrorMessage)
}

func TestDeleteAccountSuccess(t *testing.T){
	id := uuid.New().String()
	organisationId := uuid.New().String()
	accRequest := form3.CreateAccountRequest{
		Data: form3.AccountRequestData{
			Id: id,
			OrganisationId: organisationId,
			Type: "accounts",
			Attributes: form3.AccountRequestAttributes{
				Country: "GB",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}

	client := form3.ConcreteClient()
	form3.CreateAccount(&client, &accRequest)
	response, err := form3.DeleteAccount(&client, id, "0")

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, "success", response.Message)
}

// THIS TEST SHOULD HAVE RETURNED 404 but does not, i think because of incomplete demo API?
//func TestDeleteAccountDocumentNotExisting(t *testing.T){
//	client := form3.Client{}
//	response, err := form3.DeleteAccount(&client, "95f9a8b0-5926-476c-bf51-6680da49de79", "0")
//	assert.Nil(t, response)
//	assert.NotNil(t, err)
//	assert.EqualValues(t, 404, err.StatusCode)
//}

func TestFetchAccountSuccess(t *testing.T){
	id := uuid.New().String()
	organisationId := uuid.New().String()
	accRequest := form3.CreateAccountRequest{
		Data: form3.AccountRequestData{
			Id: id,
			OrganisationId: organisationId,
			Type: "accounts",
			Attributes: form3.AccountRequestAttributes{
				Country: "GB",
				BankId: "400300",
				BankIdCode: "GBDSC",
				Bic: "NWBKGB22",
			},
		},
	}

	client := form3.ConcreteClient()
	form3.CreateAccount(&client, &accRequest)
	response, err := form3.FetchAccount(&client, id)

	assert.Nil(t, err)
	assert.NotNil(t, response)
	assert.EqualValues(t, id, response.Data.Id)
	assert.EqualValues(t, organisationId, response.Data.OrganisationId)
	assert.EqualValues(t, 0, response.Data.Version)
	assert.EqualValues(t, "NWBKGB22", response.Data.Attributes.Bic)
	assert.EqualValues(t, "GB", response.Data.Attributes.Country)
	//...
}

func TestFetchAccountInvalidId(t *testing.T){
	client := form3.ConcreteClient()
	response, err := form3.FetchAccount(&client, "1")

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 400, err.StatusCode)
	assert.EqualValues(t, "id is not a valid uuid", err.ErrorMessage)
}

func TestFetchAccountNonexistentResource(t *testing.T){
	id := uuid.New().String()

	client := form3.ConcreteClient()
	response, err := form3.FetchAccount(&client, id)

	assert.Nil(t, response)
	assert.NotNil(t, err)
	assert.EqualValues(t, 404, err.StatusCode)
	assert.EqualValues(t, fmt.Sprintf("record %s does not exist" ,id), err.ErrorMessage)
}
