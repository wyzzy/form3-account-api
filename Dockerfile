FROM golang:alpine

ENV GO111MODULE=auto
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64

WORKDIR /code

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

CMD go test ./...